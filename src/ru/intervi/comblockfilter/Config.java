package ru.intervi.comblockfilter;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;

public class Config {
    private final Main MAIN;
    public List<String> list;
    public boolean enabled, regex, log, whiteList;
    public String replace;

    public Config(Main main) {
        MAIN = main;
    }

    public void load() {
        MAIN.saveDefaultConfig();
        MAIN.reloadConfig();
        FileConfiguration conf = MAIN.getConfig();
        enabled = conf.getBoolean("enabled");
        regex = conf.getBoolean("regex");
        log = conf.getBoolean("log");
        whiteList = conf.getBoolean("whitelist");
        replace = conf.getString("replace");
        list = conf.getStringList("list");
    }
}
