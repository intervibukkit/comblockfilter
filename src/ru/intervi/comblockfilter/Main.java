package ru.intervi.comblockfilter;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.CommandBlock;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.minecart.CommandMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.*;

public class Main extends JavaPlugin implements Listener {
    private final Config CONFIG = new Config(this);
    private final Logger LOGGER = Logger.getLogger("ComBlockFilter");

    @Override
    public void onLoad() {
        setupLogger();
    }

    @Override
    public void onEnable() {
        CONFIG.load();
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onRedstone(BlockRedstoneEvent event) {
        if (!CONFIG.enabled || CONFIG.list.isEmpty()) return;
        Material type = event.getBlock().getType();
        if (!type.equals(Material.COMMAND) && !type.equals(Material.COMMAND_MINECART)) return;
        BlockState state = event.getBlock().getState();
        CommandBlock commandBlock = null;
        CommandMinecart commandMinecart = null;
        String command = null;
        if (state instanceof CommandBlock) {
            commandBlock = (CommandBlock) state;
            command = commandBlock.getCommand().toLowerCase();
        } else if (state instanceof CommandMinecart) {
            commandMinecart = (CommandMinecart) state;
            command = commandMinecart.getCommand().toLowerCase();
        }
        if (command == null || command.trim().isEmpty()) return;
        String originalCom = command;
        for (int i = 0; i < CONFIG.list.size(); i++) {
            String str = CONFIG.list.get(i);
            if (CONFIG.whiteList && (
                    (CONFIG.regex && command.matches(str)) ||
                            (!CONFIG.regex && command.contains(str.toLowerCase())))
                    ) {
                return;
            } else if (i + 1 == CONFIG.list.size() &&
                    ((!CONFIG.whiteList && ((CONFIG.regex && !command.matches(str)) || (!CONFIG.regex &&
                            !command.contains(str.toLowerCase())))
                    ))) {
                return;
            }
        }
        if (commandBlock != null) commandBlock.setCommand(CONFIG.replace);
        else commandMinecart.setCommand(CONFIG.replace);
        event.setNewCurrent(event.getOldCurrent());
        state.update(true, true);
        if (!CONFIG.log) return;
        Location loc = state.getLocation();
        String mess = (
                "Bad command block in [" + loc.getWorld().getName() + ' ' + loc.getBlockX()
                        + ' ' + loc.getBlockY() + ' ' + loc.getBlockZ() + "], attempt execute: " + originalCom
        );
        LOGGER.warning(mess);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("combf.use") && !(sender instanceof ConsoleCommandSender)) {
            sender.sendMessage("no permission");
            return true;
        }
        if (args == null || args.length == 0) {
            sender.sendMessage("========= ComBlockFilter =========");
            sender.sendMessage("/combf reload - reload config");
            sender.sendMessage("/combf enable - enable plugin");
            sender.sendMessage("/combf disable - disable plugin");
            sender.sendMessage("/combf setcmd world x y z command - set command in command block");
            return true;
        }
        switch(args[0].toLowerCase()) {
            case "reload":
                CONFIG.load();
                sender.sendMessage("[ComBlockFilter] config reloaded");
                break;
            case "enable":
                getConfig().set("enable", true);
                saveConfig();
                CONFIG.load();
                sender.sendMessage("[ComBlockFilter] plugin enabled");
                break;
            case "disable":
                getConfig().set("enable", false);
                saveConfig();
                CONFIG.load();
                sender.sendMessage("[ComBlockFilter] plugin disabled");
                break;
            case "setcmd":
                String r = "[0-9-]*";
                if (args.length < 6 || !args[2].matches(r) || !args[3].matches(r) || !args[4].matches(r)) {
                    sender.sendMessage("wrong command");
                    break;
                }
                World world = Bukkit.getWorld(args[1]);
                if (world == null) {
                    sender.sendMessage("wrong world");
                    break;
                }
                String newCommand = "";
                for (int i = 5; i < args.length; i++) newCommand += args[i] + ' ';
                newCommand = newCommand.trim();
                BlockState state = world.getBlockAt(
                        Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4])
                ).getState();
                CommandBlock commandBlock = null;
                CommandMinecart commandMinecart = null;
                if (state instanceof CommandBlock) commandBlock = (CommandBlock) state;
                else if (state instanceof CommandMinecart) commandMinecart = (CommandMinecart) state;
                if (commandBlock != null) {
                    commandBlock.setCommand(newCommand);
                    sender.sendMessage("Set command: " + commandBlock.getCommand());
                } else if (commandMinecart != null) {
                    commandMinecart.setCommand(newCommand);
                    sender.sendMessage("Set command: " + commandMinecart.getCommand());
                } else sender.sendMessage("error, not get command block or command minecart");
                break;
            default:
                return false;
        }
        return true;
    }

    private void setupLogger() {
        try {
            for (Handler h : LOGGER.getHandlers()) {
                if (h instanceof FileHandler) return;
            }
            FileHandler handler = new FileHandler(
                    new File(getDataFolder(), "attempts.log").getAbsolutePath(),
                    true
            );
            handler.setFormatter(new LogFormatter("dd-MM-yyyy HH:mm:ss"));
            LOGGER.addHandler(handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class LogFormatter extends Formatter {
        private final String FORMAT;

        public LogFormatter(String format) {
            FORMAT = format;
        }

        @Override
        public String format(LogRecord record) {
            SimpleDateFormat logTime = new SimpleDateFormat(FORMAT);
            Calendar cal = new GregorianCalendar();
            cal.setTimeInMillis(record.getMillis());
            return "[" + record.getLevel() + "] " + logTime.format(cal.getTime()) + " -> " + record.getMessage() + "\n";
        }
    }
}
