# ComBlockFilter

For server versions >= **1.11.2**

Plugin for blocking execute commands with command block. No case sensitive.


**Features**:

* use regex or contains;
* black list or white list;
* print attempts;


**Commands**:

* **/combf reload** - reload config
* **/combf enable** - enable plugin
* **/combf disable** - disable plugin
* **/combf setcmd world x y z command** - set command in command block


**Permissions**:

* **combf.use** - for using commands